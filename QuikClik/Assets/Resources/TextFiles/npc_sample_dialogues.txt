default=Hello World!!
broken_npc=I used to be an adventurer like you, until I took an arrow to the knee.
game_of_thrones_npc1=Winter is coming.
game_of_thrones_npc2=The night is dark and full of terrors.
game_of_thrones_npc3=You know nothing, Jon Snow.
game_of_thrones_npc4=A Lannister always pays his debts.
game_of_thrones_npc5=HODOR HODOR HODOR HODOR...
