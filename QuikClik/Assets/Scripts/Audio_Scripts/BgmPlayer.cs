﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// BgmPlayer controls the audio for playing bgm
// attach this to main camera preferably
public class BgmPlayer : MonoBehaviour {

    public AudioManager audioManager;
    public string clipName;
    public bool doSwap = false;

    // Use this for initialization
    void Start () {
        audioManager = GameManager.audioManager;
        clipName = "bgm_sample_theme";
        audioManager.playNewBgm(clipName);      // call audioManager to handle the swap
    }

    void Update()
    {
        // trigger event to swap
        if (Input.GetKeyDown(KeyCode.K))
            doSwap = true;

        if (doSwap)
        {
            // if current clip is sample_theme, then switch to sample_enemy, use sample_theme
            clipName = (clipName.Equals("bgm_sample_theme")) ? "bgm_sample_enemy" : "bgm_sample_theme";
            audioManager.playNewBgm(clipName);      // call audioManager to handle the swap
            doSwap = false;                         // reset doSwap
        }
    }
}
