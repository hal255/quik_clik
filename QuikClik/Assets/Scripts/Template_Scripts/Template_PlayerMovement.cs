﻿/*
 * Template_PlayerMovement is a sample player movement from a previous project
 * Used as reference for player movement.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Template_PlayerMovement : MonoBehaviour {

    public const float baseSpeed = 6.0f;

    // player movement speed variables
    public float speed = 6.0f;
    public float shift_speed_offset = 2.0f;
    private bool left_shift_down = false;

    private CharacterController _charController;

    // jumping variables
    public float gravity = -9.8f;
    public float jump_speed = 15.0f;
    public float start_y = 0;
    public float previous_y = 0;
    public float current_y = 0;
    public bool canJump = true;
    public float gravity_offset = 0.5f;

    private Hashtable gravity_table;

    private bool game_paused = false;

    void Start()
    {
        _charController = GetComponent<CharacterController>();
        gravity_table = new Hashtable();
        gravity_table.Add("gravity", -9.8f);
        gravity_table.Add("current_y", 0.0f);
        gravity_table.Add("previous_y", 0.0f);
        gravity_table.Add("start_y", 0.0f);
    }

    void Update()
    {
        //transform.Translate(Input.GetAxis("Horizontal") * speed * Time.deltaTime, 0, Input.GetAxis("Vertical") * speed * Time.deltaTime);
        float deltaX = Input.GetAxis("Horizontal") * speed;
        float deltaZ = Input.GetAxis("Vertical") * speed;
        Vector3 movement = new Vector3(deltaX, 0, deltaZ);
        movement = Vector3.ClampMagnitude(movement, speed);

        // left shift held down, extra run speed
        if (Input.GetKeyDown(KeyCode.LeftShift))
            shiftSpeedChanged(shift_speed_offset);
        else if (Input.GetKeyUp(KeyCode.LeftShift))
            shiftSpeedChanged(1 / shift_speed_offset);
        //movement.y = gravity;
        movement *= Time.deltaTime;
        movement = transform.TransformDirection(movement);
        _charController.Move(movement);
        //transform.position = movement;
    }

    private void shiftSpeedChanged(float value)
    {
        speed = speed * value;
    }

    private void OnSpeedChanged(float value)
    {
        speed = baseSpeed * value;
    }

    public void setGravity(float value)
    {
        gravity = value;
    }

    public float getGravity()
    {
        return gravity;
    }

    public void setMaxHeight(float value)
    {
        jump_speed = value;
    }

    public float getMaxHeight()
    {
        return jump_speed;
    }

    private void gamePaused()
    {
        game_paused = true;
    }

    private void gameResumed()
    {
        game_paused = false;
    }
}
