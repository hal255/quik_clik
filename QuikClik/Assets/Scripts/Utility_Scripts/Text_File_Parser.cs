﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Text_File_Parser : MonoBehaviour {

    public static string resourceFolder = "TextFiles/";
    public static TextAsset textFile;
    public static string text = "";

    public static string getText(Dictionary<string, string> textDict, string key)
    {
        // make sure textDict is not null, else return error message
        if (textDict != null)
        {
            // if key in map, return it, else return error message
            if (textDict.ContainsKey(key))
                return textDict[key];
        }
        string errorMsg = "Error Text_File_Parser.getText(), textDict is null.";
        Debug.LogError(errorMsg);
        return errorMsg;
    }

    public static Dictionary<string, string> createStringDictionaryFromFile(string fileName)
    {
        // use provided fileName to find text file, and save it as TextAsset for Unity to use
        textFile = Resources.Load(resourceFolder + fileName) as TextAsset;

        // if text file exists then continue, otherwise log error message and return null
        try
        {
            string[] fileLines = textFile.text.Split('\n');     // get file line by line
            Dictionary<string, string> newDict = new Dictionary<string, string>();

            // traverse through file, one line at a time
            // if line has a key value pair via equal sign, then add to dictionary
            // return dictionary after done reading all the lines
            foreach (string s in fileLines)
            {
                if (s.Length > 0)
                {
                    string[] pairValues = s.Split('=');
                    newDict.Add(pairValues[0], pairValues[1]);
                }
            }
            return newDict;
        }
        catch(System.Exception e)
        {
            Debug.LogError("Error Text_File_Parser.createStringDictionaryFromFile() with fileName [" + fileName + "].");
            Debug.LogError(e.Message);
            return null;
        }
    }

    public static Dictionary<string, string> createStringDictionaryFromCsvFile(string fileName)
    {
        // use provided fileName to find text file, and save it as TextAsset for Unity to use
        textFile = Resources.Load(resourceFolder + fileName) as TextAsset;

        // if text file exists then continue, otherwise log error message and return null
        try
        {
            string[] fileLines = textFile.text.Split('\n');     // get file line by line
            Dictionary<string, string> newDict = new Dictionary<string, string>();

            // skip header row
            for (int i = 1; i < fileLines.Length; i++)
            {
                string s = fileLines[i];
                string[] pairValues = s.Split(',');

                // Stage,number,Enemy,health,gold,idle_image,hit_image,dead_image, 
                newDict.Add(pairValues[0],      // key value
                    pairValues[2] + "," +       // enemy name
                    pairValues[3] + "," +       // enemy health
                    pairValues[4] + "," +       // enemy gold
                    pairValues[5] + "," +       // enemy idle sprite
                    pairValues[6] + "," +       // enemy hit sprite
                    pairValues[7]);             // enemy dead sprite
            }
            return newDict;
        }
        catch (System.Exception e)
        {
            Debug.LogError("Error Text_File_Parser.createStringDictionaryFromCsvFile() with fileName [" + fileName + "].");
            Debug.LogError(e.Message);
            return null;
        }
    }
}
