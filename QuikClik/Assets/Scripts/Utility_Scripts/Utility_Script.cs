﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Utility_Script: MonoBehaviour {

    public static GameObject generateNewGameObjectWithParent(GameObject newPrefab, GameObject parent, float x, float y)
    {
        try
        {
            GameObject new_object = Instantiate(newPrefab) as GameObject;   // create new object
            new_object.transform.SetParent(parent.transform);               // attach new object to parent
            new_object.transform.localPosition = new Vector2(x, y);         // relocate new object with local coordinates
            return new_object;
        }
        catch(Exception e)
        {
            Debug.LogError("Utility_Script.generateNewGameObjectWithParent, failed to link prefab with parent");
            return null;
        }
    }

    public static Sprite generate2dSprite(Texture2D texture)
    {
        try
        {
            Rect rect = new Rect(0, 0, texture.width, texture.height);
            Vector2 pivot = new Vector2(0.5f, 0.5f);
            return Sprite.Create(texture, rect, pivot);
        }
        catch (Exception e)
        {
            Debug.LogError("Utility_Script.generate2dSprite, failed to generate sprite with texture");
            return null;
        }
    }
}
