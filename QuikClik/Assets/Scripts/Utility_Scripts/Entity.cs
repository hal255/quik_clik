﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public abstract class Entity : MonoBehaviour {
    
    public Image imageBody;
    public Button btn;

    private void Awake()
    {
        imageBody = GetComponent<Image>();
        btn = GetComponent<Button>();
    }

}
