﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {
    [SerializeField]
    Button _buffBtn;

    public AudioManager audioManager;                   // use AudioManager to control BGMs
    public PlayerManager playerManager;                 // use to control player
    public string prevClip;                             // keep track of previous clip, when out of combat or death
    public long playerDamage = 10;
    public long playerIdleDamage = 0;

    private string ENEMY_CLIP = "bgm_sample_enemy";
    private string THEME_CLIP = "bgm_sample_theme";

    //Use this for initialization
    void Start () {
        playerManager = GameManager.playerManager;
        playerManager.setPlayer(this);
        playerManager.respawn();

        audioManager = GameManager.audioManager;
        prevClip = audioManager.currentClip;
        _buffBtn.onClick.AddListener(delegate { buffTenFoldDamage(); });
    }

    public void setPlayerInCombat()
    {
        // if enemy audio clip is not playing, play it
        if (!audioManager.currentClip.Equals(ENEMY_CLIP))
        {
            prevClip = audioManager.currentClip;
            audioManager.currentClip = ENEMY_CLIP;
            audioManager.playNewBgm(ENEMY_CLIP);
        }
    }

    public void setPlayerOutCombat()
    {
        // if theme audio clip is not playing, play it
        if (!audioManager.currentClip.Equals(THEME_CLIP))
        {
            prevClip = audioManager.currentClip;
            audioManager.currentClip = THEME_CLIP;
            audioManager.playNewBgm(THEME_CLIP);
        }
    }

    public void setPlayerDamage(long value)
    {
        playerDamage = value;
    }

    public long getPlayerDamage()
    {
        return playerDamage;
    }

    public long getPlayerIdleDamage()
    {
        return playerIdleDamage;
    }

    public void buffTenFoldDamage()
    {
        playerDamage *= 10;
        playerIdleDamage *= 10;
    }
}
