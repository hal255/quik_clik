/*
 * ManagerInterface is one of two parts to manager classes
 * This requires manager classes, except GameManager, to include the functions listed in this interface
 */ 

public interface ManagerInterface {
	ManagerStatus status {get;}
    string managerObjectName { get; }

	void Startup(); //(NetworkService service);
}
