﻿/*
 * AudioManager is a manager class that controls the audio
 * Audio includes background music, and sound effects
 * Data of audio list is found in a pre-typed text file.
 * File text processing is done using Text_File_Parser.cs, and AudioManager stores the result dictionary. 
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour, ManagerInterface {

    // manager interface elements
    public ManagerStatus status { get; private set; }
    public string managerObjectName { get; private set; }

    // audio manager elements
    public AudioSource playerAudioSource; // player's audio source
    public string audioDirectory = "AudioFiles/";   // location of folder for audio files
    public int delayTime = 4;                       // number of seconds (also iterations) to reduce volume before swapping bgm
    public float defaultVolume = 0.5f;              // start at middle volume
    //public Dictionary<string, string> clipList;     // list containing the audioclips
    public string currentClip;          // tracks current song
    public float volumeSpeed;                       // increments for reducing/increasing volume

    public void Startup()
    {
        Debug.Log("Audio Manager is starting...");

        playerAudioSource = Camera.main.GetComponent<AudioSource>();
        currentClip = "bgm_sample_enemy";
        volumeSpeed = defaultVolume / (delayTime + 1);      // ensure it has some volume when reducing volume
        managerObjectName = gameObject.name;
        playNewBgm(currentClip);

        status = ManagerStatus.Started;
        Debug.Log("Audio Manager finished start up process...");
    }

    public void playNewBgm(string new_bgm)
    {
        currentClip = new_bgm;          // swap audio clip
        StartCoroutine(fadeVolume());   // after 4 seconds, swap clip and play normal volume
    }

    private IEnumerator fadeVolume()
    {
        // fade music if it's not enemy music
        if (!currentClip.Equals("bgm_sample_enemy"))
        {
            for (int i = delayTime; i > 0; i--)
            {
                playerAudioSource.volume -= volumeSpeed;
                yield return new WaitForSeconds(1);
            }
        }
        if (GameManager.DEBUG)
            Debug.Log("Playing song: " + currentClip);
        playerAudioSource.clip = getAudioClip(currentClip);
        playerAudioSource.Play();                       // play audio
        playerAudioSource.loop = true;
        playerAudioSource.volume = defaultVolume;       // set volume back up
    }

    public AudioClip getAudioClip(string clip_name)
    {
        return Resources.Load(audioDirectory + clip_name) as AudioClip;
    }
}
