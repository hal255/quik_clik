﻿
/* GameManager is the master manager class, handles all other manager classes
 * Starts all other Manager classes
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// make sure all manager classes are present and working
[RequireComponent(typeof(PlayerManager))]           // handles player object, such as health
[RequireComponent(typeof(AudioManager))]
[RequireComponent(typeof(InputManager))]
[RequireComponent(typeof(EnemyManager))]

public class GameManager : MonoBehaviour {
    public static PlayerManager playerManager { get; private set; }
    public static AudioManager audioManager { get; private set; }
    public static InputManager inputManager { get; private set; }
    public static EnemyManager enemyManager { get; private set; }

    private List<ManagerInterface> _startSequence;              // get all Manager classes to follow same interface

    public static bool DEBUG = true;                                   // one place to determine all console print statements

    void Awake()
    {
        DontDestroyOnLoad(gameObject);                              // ensure GameManager persists on all scenes

        // instantiate managers
        playerManager = GetComponent<PlayerManager>();
        audioManager = GameObject.FindObjectOfType<AudioManager>();
        inputManager = GameObject.FindObjectOfType<InputManager>();
        enemyManager = GameObject.FindObjectOfType<EnemyManager>();

        // prepare all manager classes to startup sequence
        _startSequence = new List<ManagerInterface>();
        _startSequence.Add(playerManager);
        _startSequence.Add(audioManager);
        _startSequence.Add(inputManager);
        _startSequence.Add(enemyManager);

        StartCoroutine(StartupManagers());  // run StartUp function for all managers
    }

    // StartUpManagers traverses through list of all Manager classes and starts them
    // We can use this to inform user on progress of loading scene while attempting to load all managers
    private IEnumerator StartupManagers()
    {
        foreach (ManagerInterface manager in _startSequence)
            manager.Startup(); //(network);

        int numModules = _startSequence.Count;      // total number of manager classes with startup functions
        int numReady = 0;                           // tracks how many successfully started up

        // keep trying until all manager classes are started
        while (numReady < numModules)
        {
            int lastReady = numReady;
            numReady = 0;

            // traverse through list manager classes and get updated number of started manager classes
            foreach (ManagerInterface manager in _startSequence)
            {
                if (manager.status == ManagerStatus.Started)
                    numReady++;
                // set time delay to show progress moving
                Debug.Log("Progress: " + numReady + "/" + numModules);
                yield return new WaitForSeconds(2.0f);
            }

            // update status only if additional manager classes have started
            if (numReady > lastReady)
                Debug.Log("Progress: " + numReady + "/" + numModules);
        }

        Debug.Log("All managers started up");
    }
}
