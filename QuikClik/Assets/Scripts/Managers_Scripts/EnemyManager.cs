﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyManager : MonoBehaviour, ManagerInterface {

    // manager interface elements
    public ManagerStatus status { get; private set; }
    public string managerObjectName { get; private set; }

    [SerializeField]
    GameObject _enemy;
    public GameObject ui_shakeable;
    public Dictionary<string, string> enemyData;
    public List<string> enemyList;
    public int enemyIndex = 0;
    public string enemyResourcePath;

    public Texture2D idleTexture;
    public Texture2D hitTexture;
    public Texture2D deadTexture;

    public void Startup()
    {
        Debug.Log("Enemy Manager is starting...");
        enemyResourcePath = "ImageFiles/EnemySprites/";
        ui_shakeable = GameObject.FindGameObjectWithTag("Shakeable");
        populateEnemyList();
        generateNewEnemy();
        status = ManagerStatus.Started;
        Debug.Log("Enemy Manager finished start up process...");
    }

    public void populateEnemyList()
    {
        // Stage=Enemy,health,gold,idle_image,hit_image,dead_image
        enemyData = new Dictionary<string, string>();
        enemyData = Text_File_Parser.createStringDictionaryFromCsvFile("enemy_stats");

        enemyList.Clear();
        foreach (string s in enemyData.Keys)
            enemyList.Add(s);
        //if (GameManager.DEBUG)
        //{
        //    Debug.Log("Enemy List contains: ");
        //    foreach (string s in enemyList)
        //        Debug.Log("Enemy [" + s + "]");
        //}
    }

    public void generateNewEnemy()
    {
        if (GameManager.DEBUG)
            Debug.Log("Generating new enemy");
        GameObject new_enemy_object = Utility_Script.generateNewGameObjectWithParent(_enemy, ui_shakeable, 0f, 0f);
        if (new_enemy_object || enemyIndex > enemyList.Count)
        {
            Enemy new_enemy = new_enemy_object.GetComponent<Enemy>();

            // Stage=number,Enemy,health,gold,idle_image,hit_image,dead_image, 
            string[] enemyParts = enemyData[enemyList[enemyIndex]].Split(',');
            string enemyName = enemyParts[0];
            long enemyHp = long.Parse(enemyParts[1]);
            long enemyGold = long.Parse(enemyParts[2]);

            string idleImagePath = enemyResourcePath + enemyParts[3];
            idleTexture = Resources.Load(idleImagePath) as Texture2D;
            Sprite idleImg = Utility_Script.generate2dSprite(idleTexture);
            if (idleImg == null)
                Debug.LogError("EnemyManager.generateNewEnemy: failed to get idle sprite for " + enemyName);

            string hitImagePath = enemyResourcePath + enemyParts[4];
            hitTexture = Resources.Load(hitImagePath) as Texture2D;
            Sprite hitImg = Utility_Script.generate2dSprite(hitTexture);
            if (idleImg == null)
                Debug.LogError("EnemyManager.generateNewEnemy: failed to get hit sprite" + enemyName);

            string deadImagePath = enemyResourcePath + enemyParts[5];
            deadTexture = Resources.Load(deadImagePath) as Texture2D;
            Sprite deadImg = Utility_Script.generate2dSprite(deadTexture);
            if (idleImg == null)
                Debug.LogError("EnemyManager.generateNewEnemy: failed to get dead sprite" + enemyName);

            if (GameManager.DEBUG)
            {
                Debug.Log("idle image path : " + idleImagePath);
                Debug.Log("hit image path : " + hitImagePath);
                Debug.Log("dead image path : " + deadImagePath);
            }
            new_enemy_object.name = enemyName;

            new_enemy.setEnemy(enemyName, enemyHp, enemyGold, idleImg, hitImg, deadImg, new_enemy_object.GetComponent<Image>());
            enemyIndex++;
        }
        else
            Debug.LogError("EnemyManager.generateNewEnemy: Not able to create new enemy object");
    }

}
