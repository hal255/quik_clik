﻿/*
 * ManagerStatus provides status of a Manager class via enum type
 */

public enum ManagerStatus {
	Shutdown,
	Initializing,
	Started
}