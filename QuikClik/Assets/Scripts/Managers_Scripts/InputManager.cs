﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour, ManagerInterface {

    public GameObject enemy;    // enemy on screen
    public Image enemyImage;    // enemy image

    public Vector2 rayPos;
    public RaycastHit2D hit;

    public ManagerStatus status { get; private set; }
    public string managerObjectName { get; private set; }

    public void Startup()
    {
        Debug.Log("Input manager starting...");

        //enemy = GameObject.FindGameObjectWithTag("Enemy");
        //enemyImage = enemy.GetComponent<Image>();

        // any long-running startup tasks go here, and set status to 'Initializing' until those tasks are complete
        status = ManagerStatus.Started;
        managerObjectName = gameObject.name;
        Debug.Log("InputManager, parent object: : " + managerObjectName);
    }

 //   // Update is called once per frame
 //   void Update () {
 //       // if left button is clicked, continue actions
	//	if (Input.anyKeyDown)
 //       {
 //           if (GameManager.DEBUG)
 //               Debug.Log("Mouse position: " + Input.mousePosition);

 //           //Converting Mouse Pos to 2D (vector2) World Pos
 //           Vector2 rayPos = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, 
 //               Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
 //           if (GameManager.DEBUG)
 //               Debug.Log("rayPos: " + rayPos);

 //           hit = Physics2D.Raycast(rayPos, Vector2.zero, 0f);

 //           if (hit)
 //               if (GameManager.DEBUG)
 //                   Debug.LogError("hit target: " + hit.transform.name);
 //       }
	//}

}
