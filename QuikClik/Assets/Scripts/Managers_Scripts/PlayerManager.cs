/*
 * PlayerManager incorporates the Manager Structure for the player object
 * Informs parent manager (GameManager) of status
 * Manages data such as player health, and respawn location
 * Also manages starting up the player class
 */

 
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerManager : MonoBehaviour, ManagerInterface {
    private Player _player;

	public ManagerStatus status {get; private set;}
    public string managerObjectName { get; private set; }

    public Player Player { get { return _player; } }

    public int health { get; set; }
    public int maxHealth { get; set; }
    public int ability { get; set; }


    public void Startup () { //(NetworkService service) {
		Debug.Log("Player manager starting...");

        if (!_player || _player == null)
            _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		updateData(100, 100, 0);

		// any long-running startup tasks go here, and set status to 'Initializing' until those tasks are complete
		status = ManagerStatus.Started;
        managerObjectName = gameObject.name;
        Debug.Log("PlayerManager, parent object: : " + managerObjectName);
    }



    /*/---------------------------------------------------------------------------\ \
                                   DATA METHODS
    \ \---------------------------------------------------------------------------/*/

    //gets the player we are managing
    public void setPlayer(Player player)
    {
        _player = player;
    }


    // updates player data
    public void updateData(int health, int maxHealth, int ability) {
		this.health = health;
        this.maxHealth = maxHealth;
        this.ability = ability;
	}

 

    // updates player health when damaged or replenished health
	public void ChangeHealth(int value) {
        this.health += value;
        this.health = (this.health > this.maxHealth) ? this.maxHealth : this.health;     // make sure health can't go over maxHealth

        // if player's HP hits 0 or under, broadcast it
        if (this.health <= 0)
        {
            respawn();
        }
        //Messenger.Broadcast(GameEvent.HEALTH_UPDATED);
    }

    // controls respawning, restoring health, and remove abilities
    public void respawn() {
		updateData(maxHealth, maxHealth, 0);
	}
   
}
