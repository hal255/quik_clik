﻿/*
 * DialogueManager is a manager class that controls the dialogues
 * Data of dialogue is found in a pre-typed dialogue text file.
 * File text processing is done using Text_File_Parser.cs, and DialogueManager stores the result dictionary. 
 * DialogueManager returns text from given key pair, or error message if not found.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour, ManagerInterface {

    // manager interface elements
    public ManagerStatus status { get; private set; }
    public string managerObjectName { get; private set; }

    // dialogueManager variables
    public string dialogueFileName = "";
    public Dictionary<string, string> dialogueDict;
    public string errorMsg = "";
    public Dictionary<string, Text> uiTexts;

    public void Startup()
    {
        Debug.Log("Dialogue Manager is starting...");
        dialogueFileName = "npc_sample_dialogues";
        dialogueDict = Text_File_Parser.createStringDictionaryFromFile(dialogueFileName);
        if (GameManager.DEBUG && dialogueDict == null)
            errorMsg = "Error DialogueManager.Startup(), not able to read fileName [" + dialogueFileName + "]";
        status = ManagerStatus.Started;
        managerObjectName = gameObject.name;
        mapUiTexts();
        Debug.Log("Dialogue Manager finished start up process...");
    }

    // retrieve dialogue text from dictionary
    public string getDialogueText(string key)
    {
        try
        {
            string tempStr = dialogueDict[key];
            return tempStr;
        }
        catch (System.Exception)
        {
            if (GameManager.DEBUG)
                Debug.LogError(errorMsg);
            return errorMsg;
        }
    }

    public void mapUiTexts()
    {
        uiTexts = new Dictionary<string, Text>();
        Text[] tempTexts = GameObject.FindObjectsOfType<Text>();
        foreach (Text t in tempTexts)
            uiTexts.Add(t.name, t);
    }

    public void setDialogueText(string text, string uiTextName)
    {
        uiTexts[uiTextName].text = text;
    }
}
