﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Test_EnemyController tests the functions within the class EnemyController
public class Test_EnemyController : MonoBehaviour {

    private bool DEBUG = true;          // display debug messages on Unity UI if true
    private NpcController enemy_tester;     // tests enemyController class
    void Start()
    {
        if (DEBUG)
        {
            enemy_tester = new NpcController();
            testFindPlayer();
        }
    }

    public void testFindPlayer()
    {
        Player player = enemy_tester.findPlayer();
        if (player == null)
            Debug.LogError(string.Format("Test_EnemyController.testFindPlayer: enemy {0} failed to find player", gameObject.name));
        else
            Debug.Log(string.Format("Test_EnemyController.testFindPlayer: {0} found {1}", gameObject.name, player.name));
    }

    public void testIsPlayerClose()
    {
        Player player = enemy_tester.findPlayer();
        if (player)
        {
            // track current location of gameobject and player
            Transform player_transform = player.transform;
            Vector3 enemy_old_pos = new Vector3(transform.position.x, transform.position.y, transform.position.z);

            // validate enemy did not detect player yet
            if(enemy_tester.isPlayerClose(player_transform, transform))
                Debug.LogError(string.Format("Test_EnemyController.testIsPlayerClose: enemy {0} too close to player {1}", gameObject.name, player.name));
            else
                Debug.Log(string.Format("Test_EnemyController.testIsPlayerClose: {0} is not close to {1}", gameObject.name, player.name));

            // move enemy to player
            transform.position = player_transform.position;

            // validate enemy detects player
            if (enemy_tester.isPlayerClose(player_transform, transform))
                Debug.Log(string.Format("Test_EnemyController.testIsPlayerClose: {0} is not close to {1}", gameObject.name, player.name));
            else
                Debug.LogError(string.Format("Test_EnemyController.testIsPlayerClose: enemy {0} too close to player {1}", gameObject.name, player.name));

            // move enemy back to previous position
            transform.position = enemy_old_pos;

            // validate enemy did not detect player yet
            if (enemy_tester.isPlayerClose(player_transform, transform))
                Debug.LogError(string.Format("Test_EnemyController.testIsPlayerClose: enemy {0} too close to player {1}", gameObject.name, player.name));
            else
                Debug.Log(string.Format("Test_EnemyController.testIsPlayerClose: {0} is not close to {1}", gameObject.name, player.name));
        }
    }

    private void runIsPlayerClose(Transform player_transform, string msg1, string msg2)
    {
        if (enemy_tester.isPlayerClose(player_transform, transform))
            Debug.Log(msg1);
        else
            Debug.Log(msg2);

    }
}
