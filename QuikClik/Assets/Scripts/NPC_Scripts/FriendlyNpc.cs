﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FriendlyNpc : MonoBehaviour {

    //[SerializeField] Text UI_npcText;                       // npc_text that will be modified
    [SerializeField] string npcDialogueKey;                 // key pair value to match with dialogue text file

    public float playerMaxDistance = 6.0f;                  // player max distance before detecting player
    public bool playerFound = false;
    public bool isTextDisplaying = false;                   // determines if UI Text will display friendlyText
    public bool playerMovedAway = true;                     // prevents repeated dialogue while player stands close
    public string uiTextName = "PlayerDialogueText";        // UI Text object to display NPC dialogue
    //public string dialogueKey = "default";                         // determines which dialogue text retrieved

    private NpcController npcController;                    // use general npc class to locate player
    private Player player;
    private DialogueManager dialogueManager;

    // Use this for initialization
    void Start()
    {
        npcController = new NpcController();
        //dialogueManager = GameManager.dialogueManager;
        player = npcController.findPlayer();
        npcController.setPlayerMaxDistance(playerMaxDistance);
        npcDialogueKey = (npcDialogueKey == null || npcDialogueKey.Length < 1) ? "default" : npcDialogueKey;     // make sure npcDialogue is not empty
    }

    // Update is called once per frame
    void Update()
    {
        // if player is close by, look towards player and move towards player
        if (playerFound)
        {
            // make sure player object exists
            if (player)
            {
                npcController.faceTowardsPlayer(player.transform, transform);

                // if friendlyText is not displaying and player not staying in place, then call coroutine to show message
                if (!isTextDisplaying && playerMovedAway)
                    StartCoroutine(startUIText());
                Debug.Log(string.Format("{0} is close to {1}", player.name, gameObject.name));
            }
            playerMovedAway = false;        // player now close to NPC
        }
        else
            playerMovedAway = true;        // player moved away from NPC

        // check if player is close by
        playerFound = npcController.isPlayerClose(player.transform, transform);
    }

    // updates text on the UI Text object
    // TODO: maybe store this function somewhere else
    IEnumerator addUiText(string strText, string uiTextName)
    {
        dialogueManager.setDialogueText(strText, uiTextName);
        isTextDisplaying = true;
        yield return new WaitForSeconds(3);
    }

    IEnumerator startUIText()
    {
        npcController.setNpcText(dialogueManager.getDialogueText(npcDialogueKey));       // update npc text

        // Start function addUiText as a coroutine to display text for a few seconds
        yield return StartCoroutine(addUiText(npcController.getNpcText(), uiTextName));

        // after coroutine is done, remove the text
        dialogueManager.setDialogueText("", uiTextName);
        isTextDisplaying = false;
    }

    // changing dialogueKey to change dialogue text of npc
    public void setDialogueKey(string key)
    {
        npcDialogueKey = key;
    }

    public string getDialogueKey()
    {
        return npcDialogueKey;
    }
}
