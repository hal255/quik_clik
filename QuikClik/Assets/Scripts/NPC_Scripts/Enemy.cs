﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class Enemy : Entity {

    public EnemyManager enemyManager;
    public Player _player;
    public Image _img;

    public string enemyName;
    public long health;
    public long gold;
    public Sprite idleImg;
    public Sprite hitImg;
    public Sprite deadImg;

    public bool doneIdle = true;

    void Start()
    {
        enemyManager = GameManager.enemyManager;
    }

    void Update()
    {
        if (doneIdle)
        {
            doneIdle = false;
            StartCoroutine(doIdleHit(_player.getPlayerIdleDamage()/10));
        }
    }
    // set new enemy
    // Enemy name,health,gold,idle_image,hit_image,dead_image
    public void setEnemy(string enemyName, long health, long gold, Sprite idleImg, Sprite hitImg, Sprite deadImg, Image img)
    {
        _img = img;
        this.enemyName = enemyName;
        this.health = health;
        this.gold = gold;
        this.idleImg = idleImg;
        this.hitImg = hitImg;
        this.deadImg = deadImg;

        _player = GameManager.playerManager.Player;
        btn.onClick.AddListener(delegate { damageEnemy(_player.getPlayerDamage()); });
        _img.sprite = idleImg;
        _img.SetNativeSize();
    }

    // adjust health
    public void damageEnemy(long change)
    {
        health -= change;
        health = (health < 0) ? 0 : health;     // don't go below 0
        if (health < 1)
            StartCoroutine(doDeath());
        else
            StartCoroutine(doHit());
    }

    // perform death action
    private IEnumerator doDeath()
    {
        _img.sprite = deadImg;
        _img.rectTransform.sizeDelta = new Vector2(_img.rectTransform.sizeDelta.x, _img.rectTransform.sizeDelta.y / 2);
        if (GameManager.DEBUG)
            Debug.Log("Performing Death for enemy: " + name);
        btn.enabled = false;
        yield return new WaitForSeconds(0.1f);
        enemyManager.generateNewEnemy();
        yield return new WaitForSeconds(0.1f);
        Destroy(gameObject);
    }

    // perform death action
    private IEnumerator doHit()
    {
        _img.sprite = hitImg;
        if (GameManager.DEBUG)
            Debug.Log("Performing Hit for enemy: " + name);
        yield return new WaitForSeconds(0f);
        _img.sprite = idleImg;
    }

    // perform death action
    private IEnumerator doIdleHit(long change)
    {
        health -= change;
        health = (health < 0) ? 0 : health;     // don't go below 0
        if (health < 1)
            StartCoroutine(doDeath());
        else
        {
            yield return new WaitForSeconds(0.1f);
            doneIdle = true;
        }
    }

}
