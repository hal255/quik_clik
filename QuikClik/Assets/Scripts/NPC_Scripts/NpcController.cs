﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// EnemyController is used as a template class for behavior of all enemies
public class NpcController : MonoBehaviour
{


    // variables to determine attack player or not
    public float playerMaxDistance = 5.0f;    // 5 seems good amount of space

    // max idle movement
    public float npcMaxIdleMovement = 3.0f;

    // dialogues
    public string textStr = "";

    // determine if npc is in idle or not
    public bool isIdle = true;

    // generic function to find player game object, currently 
    // TODO: will there be multiple players?
    public Player findPlayer()
    {
        Player tempPlayer;
        tempPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        return tempPlayer;
    }

    // determines if player game object is close
    public bool isPlayerClose(Transform player, Transform other)
    {
        // if player position is not null, then proceed to compare distances
        if (player && other)
        {
            float dist = Vector3.Distance(player.position, other.position);
            if (dist < playerMaxDistance)
                return true;
        }
        return false;
    }

    // get player distance, used to interact with player based on proximity
    // enemy object can attack player, while non enemy can chat with player
    // returns -1 for error handling
    public float getPlayerDistance(Transform player, Transform other)
    {
        if (player && other)
            return Vector3.Distance(player.position, other.position);
        return -1.0f;
    }

    // modify distance between player and npc
    public void setPlayerMaxDistance(float value)
    {
        playerMaxDistance = value;
    }

    // faceTowardsPlayer will turn Enemy object towards player
    public void faceTowardsPlayer(Transform player, Transform other)
    {
        other.LookAt(player);
    }

    // set npc to idle or not
    public void setIdle(bool val)
    {
        isIdle = val;
    }

    // returns idle status of NPC
    public bool getIsIdle()
    {
        return isIdle;
    }

    // set max npc idle movement
    public void setMaxIdleDistance(float val)
    {
        npcMaxIdleMovement = val;
    }

    // override npc text
    public void setNpcText(string text)
    {
        textStr = text;
    }

    // get npc dialogue
    public string getNpcText()
    {
        return textStr;
    }

}
